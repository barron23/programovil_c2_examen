package com.example.examenc2;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class VentasAdapter extends ArrayAdapter<Ventas> {

    public VentasAdapter(Context context, ArrayList<Ventas> ventasList) {
        super(context, 0, ventasList);
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // Obtener la venta en la posición actual
        Ventas venta = getItem(position);

        // Verificar si ya existe una vista que se pueda reutilizar
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        }

        // Configurar el contenido de la vista con la información de la venta
        TextView textViewVenta = convertView.findViewById(android.R.id.text1);
        if (venta != null) {
            String ventaInfo = "Cantidad: " + venta.getCantidad() +
                    " litros, Precio: $" + venta.getPrecio();
            textViewVenta.setText(ventaInfo);
        }

        return convertView;
    }

}

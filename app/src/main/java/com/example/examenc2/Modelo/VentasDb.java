package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class VentasDb{

    private Context context;
    private ventasDbHelper helper;
    private SQLiteDatabase db;

    public VentasDb(Context context, UsuarioDbHelper helper) {
        this.context = context;
        this.helper = helper;
    }

    public VentasDb(Context context) {
        this.context = context;
        this.helper = new UsuarioDbHelper(this.context);
    }

    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    @Override
    public long insertVentas(Ventas ventas) {
        ContentValues values = new ContentValues();
        values.put(DefineTable.Ventas.COLUMN_NAME_NUM_BOMBA, ventas.getNumBomba());
        values.put(DefineTable.Ventas.COLUMN_NAME_TIPO_GASOLINA, ventas.getTipoGasolina());
        values.put(DefineTable.Ventas.COLUMN_NAME_PRECIO_GASOLINA, ventas.getPrecioGasolina());
        values.put(DefineTable.Ventas.COLUMN_NAME_CANTIDAD_GASOLINA, ventas.getCantidadGasolina());

        this.openDataBase();
        long num = db.insert(DefineTable.Ventas.TABLE_NAME, null, values);
        this.closeDataBase();
        Log.d("agregar", "insertVentas: " + num);

        return num;
    }

    @Override
    public Ventas getVentas(int numBomba) {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTable.Ventas.TABLE_NAME,
                DefineTable.Ventas.REGISTRO,
                DefineTable.Ventas.COLUMN_NAME_NUM_BOMBA + " = ?",
                new String[]{String.valueOf(numBomba)},
                null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            Ventas ventas = readVentas(cursor);
            cursor.close();
            return ventas;
        }
        return null;
    }

    @Override
    public ArrayList<Ventas> allVentas() {
        this.openDataBase(); // Abre la base de datos

        Cursor cursor = db.query(
                DefineTable.Ventas.TABLE_NAME,
                DefineTable.Ventas.REGISTRO,
                null, null, null, null, null);
        ArrayList<Ventas> ventasList = new ArrayList<>();
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Ventas ventas = readVentas(cursor);
            ventasList.add(ventas);
            cursor.moveToNext();
        }

        cursor.close();

        this.closeDataBase();
        return ventasList;
    }

    @Override
    public Ventas readVentas(Cursor cursor) {
        Ventas ventas = new Ventas();
        ventas.setIdVentas(cursor.getInt(0));
        ventas.setNumBomba(cursor.getInt(1));
        ventas.setTipoGasolina(cursor.getInt(2));
        ventas.setPrecioGasolina(cursor.getFloat(3));
        ventas.setCantidadGasolina(cursor.getInt(4));
        return ventas;
        }
}
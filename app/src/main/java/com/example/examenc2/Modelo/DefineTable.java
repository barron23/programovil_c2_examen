package Modelo;

public class DefineTable {
    public DefineTable() {}

    public static abstract class Usuarios {
        // Definición de la tabla "usuarios"
        public static final String TABLE_NAME = "usuarios";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_NOMBRE_USUARIO = "nombre_usuario";
        public static final String COLUMN_NAME_CORREO = "correo";
        public static final String COLUMN_NAME_CONTRASENA = "contrasena";

        public static String[] REGISTRO = new String[]{
                Usuarios.COLUMN_NAME_ID,
                Usuarios.COLUMN_NAME_NOMBRE_USUARIO,
                Usuarios.COLUMN_NAME_CORREO,
                Usuarios.COLUMN_NAME_CONTRASENA
        };
    }

    public static abstract class Ventas {
        // Definición de la tabla "ventas"
        public static final String TABLE_NAME = "ventas";
        public static final String COLUMN_NAME_ID_VENTAS = "id_ventas";
        public static final String COLUMN_NAME_NUM_BOMBA = "num_bomba";
        public static final String COLUMN_NAME_TIPO_GASOLINA = "tipo_gasolina";
        public static final String COLUMN_NAME_PRECIO_GASOLINA = "precio_gasolina";
        public static final String COLUMN_NAME_CANTIDAD_GASOLINA = "cantidad_gasolina";

        public static String[] REGISTRO = new String[]{
                Ventas.COLUMN_NAME_ID_VENTAS,
                Ventas.COLUMN_NAME_NUM_BOMBA,
                Ventas.COLUMN_NAME_TIPO_GASOLINA,
                Ventas.COLUMN_NAME_PRECIO_GASOLINA,
                Ventas.COLUMN_NAME_CANTIDAD_GASOLINA
        };
}
}
package com.example.examenc2;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.text.DecimalFormat;
import java.util.ArrayList;
public class listaActivity {


    public class ListaActivity extends AppCompatActivity {
        private ListView listViewVentas;
        private TextView textViewTotalVentas;

        private ArrayList<Ventas> registroVentas;
        private double totalVentas;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_lista);

            listViewVentas = findViewById(R.id.listViewVentas);
            textViewTotalVentas = findViewById(R.id.textViewTotalVentas);

            Bundle extras = getIntent().getExtras();
            if (extras != null && extras.getSerializable("registroVentas") != null) {
                registroVentas = (ArrayList<Ventas>) extras.getSerializable("registroVentas");
            }
            if (registroVentas != null && registroVentas.size() > 0) {
                ArrayAdapter<Ventas> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, registroVentas);
                listViewVentas.setAdapter(adapter);
            }
            totalVentas = calcularTotalVentas();
            DecimalFormat df = new DecimalFormat("#.##");
            textViewTotalVentas.setText("Total de Ventas: $" + df.format(totalVentas));
        }

        private double calcularTotalVentas() {
            double total = 0;
            if (registroVentas != null) {
                for (Ventas venta : registroVentas) {
                    total += venta.getPrecioGasolina() * venta.getCantidadGasolina();
                }
            }
            return total;
        }
    }
}


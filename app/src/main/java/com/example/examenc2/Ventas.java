package com.example.examenc2;

public class Ventas {

    private int idVenta;
    private int numBomba;
    private int tipoGasolina;
    private float precioGasolina;
    private int cantidadGasolina;

    public Ventas(int id_venta, int num_Bomba, int tipo_Gasolina, float precio_Gasolina, int cantidad_Gasolina) {
        this.idVenta = id_venta;
        this.numBomba = num_Bomba;
        this.tipoGasolina = tipo_Gasolina;
        this.precioGasolina = precio_Gasolina;
        this.cantidadGasolina = cantidad_Gasolina;
    }

    public int getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(int id_venta) {
        this.idVenta = id_venta;
    }

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int num_Bomba) {
        this.numBomba = num_Bomba;
    }

    public int getTipoGasolina() {
        return tipoGasolina;
    }

    public void setTipoGasolina(int tipo_Gasolina) {
        this.tipoGasolina = tipo_Gasolina;
    }

    public float getPrecioGasolina() {
        return precioGasolina;
    }

    public void setPrecioGasolina(float precio_Gasolina) {
        this.precioGasolina = precio_Gasolina;
    }

    public int getCantidadGasolina() {
        return cantidadGasolina;
    }

    public void setCantidadGasolina(int cantidad_Gasolina) {
        this.cantidadGasolina = cantidad_Gasolina;
    }


}



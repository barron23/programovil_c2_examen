package com.example.examenc2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private EditText editTextNumBomba, editTextPrecio, editTextCapacidad, editTextContadorLitros, editTextCantidadLitros;
    private RadioButton radioButtonRegular, radioButtonExtra;
    private Button buttonIniciarBomba, buttonHacerVenta, buttonRegistrarVenta, buttonConsultarRegistro, buttonSalir, buttonLimpiar;
    private BombaGasolina bombaGasolina;
    private ArrayList<Ventas> registroVentas;
    private double totalVentas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inicializar las vistas
        editTextNumBomba = findViewById(R.id.editTextNumBomba);
        radioButtonRegular = findViewById(R.id.radioButtonRegular);
        radioButtonExtra = findViewById(R.id.radioButtonExtra);
        editTextPrecio = findViewById(R.id.editTextPrecio);
        editTextCapacidad = findViewById(R.id.editTextCapacidad);
        editTextContadorLitros = findViewById(R.id.editTextContadorLitros);
        editTextCantidadLitros = findViewById(R.id.editTextCantidadLitros);
        buttonIniciarBomba = findViewById(R.id.buttonIniciarBomba);
        buttonHacerVenta = findViewById(R.id.buttonHacerVenta);
        buttonRegistrarVenta = findViewById(R.id.buttonRegistrarVenta);
        buttonConsultarRegistro = findViewById(R.id.buttonConsultarRegistro);
        buttonSalir = findViewById(R.id.buttonSalir);
        buttonLimpiar = findViewById(R.id.buttonLimpiar);

        // Inicializar variables
        registroVentas = new ArrayList<>();
        totalVentas = 0.0;

        // Evento clic para el botón Iniciar Bomba
        buttonIniciarBomba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iniciarBomba();
            }
        });

        // Evento clic para el botón Hacer Ventas
        buttonHacerVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hacerVenta();
            }
        });

        // Evento clic para el botón Registrar Ventas
        buttonRegistrarVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarVenta();
            }
        });

        // Evento clic para el botón Consultar Registro de Ventas
        buttonConsultarRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                consultarRegistroVentas();
            }
        });

        // Evento clic para el botón Salir
        buttonSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmarSalir();
            }
        });


        // Evento clic para el botón Limpiar
        buttonLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarDatos();
            }
        });
    }

    // Método para asignar valores iniciales a la bombaGasolina
    private void iniciarBomba() {
        if (editTextNumBomba.getText().toString().isEmpty()
                || editTextPrecio.getText().toString().isEmpty()
                || editTextCapacidad.getText().toString().isEmpty()) {
            Toast.makeText(MainActivity.this, "Llene los campos vacíos", Toast.LENGTH_SHORT).show();
        } else {
            int numBomba = Integer.parseInt(editTextNumBomba.getText().toString());
            String tipoGasolina = radioButtonRegular.isChecked() ? "Regular" : "Extra";
            double precio = Double.parseDouble(editTextPrecio.getText().toString());
            double capacidad = Double.parseDouble(editTextCapacidad.getText().toString());

            if (capacidad > 200) {
                Toast.makeText(this, "La capacidad no debe ser mayor a 200", Toast.LENGTH_SHORT).show();
                return;
            }

            bombaGasolina = new BombaGasolina(numBomba, tipoGasolina, capacidad);
            bombaGasolina.setContadorVentas(0);
            editTextContadorLitros.setText("0.0");
            editTextCantidadLitros.setText("");
            Toast.makeText(this, "Bomba iniciada", Toast.LENGTH_SHORT).show();
        }
    }

    // Método para hacer una venta
    private void hacerVenta() {
        if (bombaGasolina == null) {
            Toast.makeText(this, "Debe iniciar la bomba antes de hacer una venta", Toast.LENGTH_SHORT).show();
            return;
        }

        double cantidadLitros = Double.parseDouble(editTextCantidadLitros.getText().toString());

        if (cantidadLitros > bombaGasolina.getCapacidad() - bombaGasolina.getContadorVentas()) {
            Toast.makeText(this, "La cantidad de gasolina excede el inventario disponible", Toast.LENGTH_SHORT).show();
            return;
        }

        double costo = cantidadLitros * Double.parseDouble(editTextPrecio.getText().toString());
        DecimalFormat df = new DecimalFormat("#.##");
        String mensaje = "Cantidad: " + df.format(cantidadLitros) + " litros\nCosto: $" + df.format(costo);
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();

        bombaGasolina.setContadorVentas(bombaGasolina.getContadorVentas() + cantidadLitros);
        editTextContadorLitros.setText(String.valueOf(bombaGasolina.getContadorVentas()));

        totalVentas += costo;
    }

    // Método para registrar la venta en el registro de ventas
    private void registrarVenta() {
        if (bombaGasolina == null) {
            Toast.makeText(this, "Debe iniciar la bomba antes de registrar una venta", Toast.LENGTH_SHORT).show();
            return;
        }

        int numBomba = Integer.parseInt(editTextNumBomba.getText().toString());
        String tipoGasolina = radioButtonRegular.isChecked() ? "Regular" : "Extra";
        double precio = Double.parseDouble(editTextPrecio.getText().toString());
        double cantidad = Double.parseDouble(editTextCantidadLitros.getText().toString());

        Ventas venta = new Ventas(numBomba, tipoGasolina, precio, cantidad);
        registroVentas.add(venta);
        Toast.makeText(this, "Venta registrada", Toast.LENGTH_SHORT).show();
    }

    // Método para abrir la actividad ListaActivity y mostrar el registro de ventas
    private void consultarRegistroVentas() {
        Intent intent = new Intent(this, ListaActivity.class);
        intent.putExtra("registroVentas", registroVentas);
        startActivity(intent);
    }

    // Método para mostrar un diálogo de confirmación antes de salir de la aplicación
    private void confirmarSalir() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirmar salida");
        builder.setMessage("¿Estás seguro de que deseas salir?");
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }

    // Método para limpiar los datos de la venta actual
    private void limpiarDatos() {
        editTextCantidadLitros.setText("");
    }
}
